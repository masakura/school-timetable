export class DayOfWeek {
  readonly label: string;

  constructor(readonly key: string, readonly fullText: string) {
    this.label = this.fullText[0] as string;
  }

  static readonly Monday = new DayOfWeek('monday', '月曜日');

  static readonly Tuesday = new DayOfWeek('tuesday', '火曜日');

  static readonly Wednesday = new DayOfWeek('wednesday', '水曜日');

  static readonly Thursday = new DayOfWeek('thursday', '木曜日');

  static readonly Friday = new DayOfWeek('friday', '金曜日');

  static get(key: string): DayOfWeek {
    switch (key) {
      case this.Monday.key: return this.Monday;
      case this.Tuesday.key: return this.Tuesday;
      case this.Wednesday.key: return this.Wednesday;
      case this.Thursday.key: return this.Thursday;
      case this.Friday.key: return this.Friday;

      default: throw new Error('Unknown day of week');
    }
  }
}
