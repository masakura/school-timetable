import { DayOfWeek } from '@/domain/day-of-week/DayOfWeek';

export class DayOfWeekCollection {
  constructor(private readonly items: DayOfWeek[]) {
  }

  asEnumerable(): DayOfWeek[] {
    return [...this.items];
  }
}
