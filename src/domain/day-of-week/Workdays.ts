import { DayOfWeekCollection } from '@/domain/day-of-week/DayOfWeekCollection';
import { DayOfWeek } from '@/domain/day-of-week/DayOfWeek';

export class Workdays {
  private readonly dayOfWeeks = new DayOfWeekCollection([
    DayOfWeek.Monday,
    DayOfWeek.Tuesday,
    DayOfWeek.Wednesday,
    DayOfWeek.Thursday,
    DayOfWeek.Friday,
  ]);

  asEnumerable(): DayOfWeek[] {
    return this.dayOfWeeks.asEnumerable();
  }
}
