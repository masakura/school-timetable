import { PeriodCollection } from '@/domain/period/PeriodCollection';
import { Period } from '@/domain/period/Period';

export class AvailablePeriods {
  private readonly periods = PeriodCollection.withNumber(6);

  keys(): number[] {
    return this.periods.keys();
  }

  asEnumerable(): Period[] {
    return this.periods.asEnumerable();
  }
}
