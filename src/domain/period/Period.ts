export class Period {
  readonly label: string;

  constructor(readonly key: number) {
    this.label = key.toString();
  }
}
