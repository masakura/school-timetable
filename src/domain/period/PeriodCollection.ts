import { Period } from '@/domain/period/Period';

export class PeriodCollection {
  constructor(private readonly items: Period[]) {
  }

  asEnumerable(): Period[] {
    return [...this.items];
  }

  keys(): number[] {
    return this.items.map((item) => item.key);
  }

  static withNumber(number: number): PeriodCollection {
    const list = Array.from({ length: number }, (_, index) => new Period(index + 1));

    return new PeriodCollection(list);
  }
}
