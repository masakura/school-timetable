import { DayOfWeek } from '@/domain/day-of-week';

const dayOfWeeks = [
  { key: 'monday', label: '月', fullText: '月曜日' },
  { key: 'tuesday', label: '火', fullText: '火曜日' },
  { key: 'wednesday', label: '水', fullText: '水曜日' },
  { key: 'thursday', label: '木', fullText: '木曜日' },
  { key: 'friday', label: '金', fullText: '金曜日' },
];

describe('PeriodOfWeek', () => {
  describe('曜日', () => {
    dayOfWeeks.forEach((dayOfWeek) => {
      test(dayOfWeek.key, () => {
        expect(DayOfWeek.get(dayOfWeek.key)).toEqual(dayOfWeek);
      });
    });
  });
});
