import { AvailablePeriods } from '@/domain/period/AvailablePeriods';

describe('AvailablePeriods', () => {
  test('有効な時限', () => {
    expect(new AvailablePeriods().keys()).toEqual([1, 2, 3, 4, 5, 6]);
  });
});
