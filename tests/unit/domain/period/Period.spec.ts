import { Period } from '@/domain/period';

describe('Period', () => {
  test('時限', () => {
    const target = new Period(4);

    expect(target).toEqual({ key: 4, label: '4' });
  });
});
