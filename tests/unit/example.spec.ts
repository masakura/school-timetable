import Vue from 'vue';
import Vuetify from 'vuetify';
import { shallowMount } from '@vue/test-utils';
import HelloWorld from '@/components/HelloWorld.vue';

Vue.use(Vuetify);

describe('HelloWorld.vue', () => {
  it('renders pages', () => {
    const wrapper = shallowMount(HelloWorld);
    expect(wrapper.text()).toContain('Welcome');
  });
});
