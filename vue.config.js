module.exports = {
  publicPath: process.env.BASE_URL,
  transpileDependencies: [
    'vuetify',
  ],
  pages: {
    index: {
      entry: 'src/main.ts',
      title: 'School Timetable',
    },
  },
};
